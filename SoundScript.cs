﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		DontDestroyOnLoad(gameObject);
		var gameMusic = GameObject.Find("GameMusic");
		if (gameMusic)
		{
			Destroy(gameMusic);
		}
		
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
