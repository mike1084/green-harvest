﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Logging;




public class CharacterScreenGUI : MonoBehaviour 
{
	public bool Connected = false;
	public bool GUIOn = true;
	public string userName = "Username";
	public string userPass = "Password";
	public Texture2D background;
	public string serverName = "84.84.125.182";		// Use Unity Inspector to change this value.
	public int serverPort = 9933;				// Use Unity Inspector to change this value.
	public LogLevel logLevel = LogLevel.DEBUG;	// Use Unity Inspector to change this value.
	public string zone = "TestWorld";
	public bool charonespawned = false;
	public bool chartwospawned = false;
	public bool charthreespawned = false;
	public GameObject charone;
	public bool response = false;
	public string names = "Here are your names!";
	//public Material CharacterOne = Resources.Load("CharacterOne", typeof(Material)) as material;
	
	private int toolbarInt = 0;
	private string[] toolbarStrings = new string[] {"Randoff Hunters", "Glimpné Frostfield", "Create new character!"};

	
	private string statusMessage = "";
	private List<string> logMessages = new List<string>();
	private Vector2 logScrollPosition;	
	
	protected SmartFox smartFox;
	public bool debug = true;
	
	
	//Set the ISFSObject for the character selection names to be put in.
	ISFSObject characterselect = new SFSObject();

	
	//Allow the program to remain working even while un-focused.
	protected void Awake(){
		Application.runInBackground = true;
	}
	//Stop any connections / callbacks.
	protected void UnregisterSFSSceneCallbacks(){
		smartFox.RemoveAllEventListeners();
		
	}
	
	// Use this for initialization.
	void Start()
	{
		
		if (Application.isWebPlayer || Application.isEditor) {
			if (!Security.PrefetchSocketPolicy(serverName, serverPort, 500)) {
				Debug.LogError("Security Exception. Policy file loading failed!");
			}
		}

		
		smartFox = new SmartFox(true);
		smartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
		smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
		smartFox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
		
		// Also register for all debug messages from the API at the given log level.
		//smartFox.AddLogListener(logLevel, OnDebugMessage);
		
		statusMessage = "Not connected (client API v" + smartFox.Version + ")";
		logMessages.Add("LOG MESSAGES");
		
		smartFox.Connect(serverName, serverPort);
		
		characterselect.PutUtfString("userid", "2");
		//Set the character selection userid (temp fix).

		//Send the information that is required for the character selection, and wait for response.
		smartFox.Send(new ExtensionRequest("characterselect", characterselect));
		
		
			
		}
		
		//So that Unity won't start messing up things; let's queue the event.
		void FixedUpdate() {
			smartFox.ProcessEvents();
	}
	
	
	
	//Here the GUI will be rendered every second.
	void OnGUI() {
		if (smartFox == null) return;
		
	// Lets just quickly set up some GUI layout variables.
	float GUIAreaLeft = Screen.width*0.05f;
	float GUIAreaRight = Screen.width*0.95f;
	float GUIAreaTop = Screen.height*0.1f;
	float GUIOverlayWidth = Screen.width*0.9f;
	float GUIOverlayHeight = Screen.height*0.9f;
	float GUIInfoBoxWidth = Screen.width/4;
	float GUIInfoBoxHeight = Screen.height*0.8f;
	float GUIInfoBoxRight = GUIOverlayWidth - GUIInfoBoxWidth;
	float GUISelectLeft = Screen.width/8;
	float GUISelectTop = Screen.height*0.825f;
	float GUISelectWidth = GUIOverlayWidth - GUIInfoBoxWidth;
	float GUISelectHeight = Screen.height*0.05f;
		


		
		
		if(GUIOn = true){
		GUILayout.BeginArea(new Rect(GUIAreaLeft,GUIAreaTop,GUIOverlayWidth,GUIOverlayHeight));
		GUI.Box(new Rect(0,0,GUIInfoBoxWidth,GUIInfoBoxHeight), "Character information", "Box");
		GUI.Box(new Rect(GUIInfoBoxRight,0,GUIInfoBoxWidth,GUIInfoBoxHeight), "Character information");
		if (response == true){
		GUI.Box(new Rect(100,100,100,300), "These are your names.");
			
		}
		toolbarInt = GUI.Toolbar (new Rect (GUISelectLeft, GUISelectTop, GUISelectWidth, GUISelectHeight), toolbarInt, toolbarStrings);
			
			
			
			
			
			

			
	//Toolbar option 1 (0-based) selected.
		if(toolbarInt == 0){
			if(!charonespawned){
			
			//Load material "CharacterOne" and extract mesh named "Shield"
				Mesh mesh = ((GameObject)Resources.Load ("Shield")).GetComponent<MeshFilter>().mesh;
				Material material = Resources.Load ("CharacterOne", typeof(Material)) as Material;
 
			//Instantiate a new game object, and add mesh components so it's visible
			//We set the sharedMesh to the mesh we extracted from the prefab and the material 
			//of the MeshRenderer component
       
				GameObject characterOne = new GameObject("Character One");
				MeshFilter meshFilter = characterOne.AddComponent<MeshFilter>();
				characterOne.transform.position = new Vector3(0f, -0.15f, -5f);
				characterOne.transform.rotation = new Quaternion(0,180,0,0);
				meshFilter.sharedMesh = mesh;
				MeshRenderer meshRenderer =characterOne.AddComponent<MeshRenderer>();
				meshRenderer.material = material;
			charonespawned = true;
			chartwospawned = false;
			Destroy(GameObject.Find("Character Two"));
				
			}
		}

		
	//Toolbar option 2	
		if (toolbarInt == 1){
			if(!chartwospawned){
				
				Mesh mesh = ((GameObject)Resources.Load ("Shield")).GetComponent<MeshFilter>().mesh;
				Material material = Resources.Load ("CharacterTwo", typeof(Material)) as Material;
				
				GameObject characterTwo = new GameObject("Character Two");
				MeshFilter meshFilter = characterTwo.AddComponent<MeshFilter>();
				characterTwo.transform.position = new Vector3(0f, -0.15f, -5f);
				characterTwo.transform.rotation = new Quaternion(0,180,0,0);
				meshFilter.sharedMesh = mesh;
				MeshRenderer meshRenderer =characterTwo.AddComponent<MeshRenderer>();
				meshRenderer.material = material;
			chartwospawned = true;
			charonespawned = false;
				
			Destroy(GameObject.Find("Character One"));
				}
		}
		
	//Toolbar option 3- Create a character!
		if (toolbarInt == 2)
		{
			Application.LoadLevel(2);			
		}
		
			
		
			
		GUILayout.EndArea();

	}
}
	
		public void OnConnection(BaseEvent evt) {
		bool success = (bool)evt.Params["success"];
		if (success) {
			Connected = true;
			statusMessage = "Connection successful!";
		} else {
			statusMessage = "Can't connect to server!";
		}
	}

	public void OnConnectionLost(BaseEvent evt) 
	{
		statusMessage = "Connection lost; reason: " + (string)evt.Params["reason"];
	}
	
	
	
	
			
	public void OnExtensionResponse(BaseEvent evt)
		{
		//string names = (string) evt.Params["characterNames"];
		response = true;
			
		}

	
	//----------------------------------------------------------
	// Show the debug messages both in window as well as console log
	//----------------------------------------------------------
	public void OnDebugMessage(BaseEvent evt) {
		string message = (string)evt.Params["message"];
		logMessages.Add(message);
		Debug.Log("[SFS DEBUG] " + message);
	}
	
	
	
	
	

	
	//----------------------------------------------------------
	// Disconnect from the socket when shutting down the game
	//----------------------------------------------------------
	public void OnApplicationQuit() {
		UnregisterSFSSceneCallbacks();
		smartFox.Disconnect();
	}
	
}