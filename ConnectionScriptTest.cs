﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Logging;
using Sfs2X.Util;




public class ConnectionScriptTest : MonoBehaviour 
{
	public bool Connected = false;
	public bool GUIOn = true;
	public string username = "Username";
	public string password = "Password";
	public string zone = "TestWorld";
	public string errorlog = "";
	public Texture2D background;
	public string serverName = "84.84.125.182";		// Use Unity Inspector to change this value.
	public int serverPort = 9933;				// Use Unity Inspector to change this value.
	public LogLevel logLevel = LogLevel.DEBUG;	// Use Unity Inspector to change this value.
	public string account_id = "";
	
	
	
	private string statusMessage = "";
	private List<string> logMessages = new List<string>();
	private Vector2 logScrollPosition;	
	
	protected SmartFox smartFox;
	public bool debug = true;

	
	//Allow the program to remain working even while un-focused.
	protected void Awake()
	{
		Application.runInBackground = true;
	}
	//Stop any 
	protected void UnregisterSFSSceneCallbacks()
	{
		smartFox.RemoveAllEventListeners();
		
	}
	
	// Use this for initialization.
	void Start()
	{
		Security.PrefetchSocketPolicy(serverName, serverPort);
		if (Application.isWebPlayer || Application.isEditor) 
		{
			if (!Security.PrefetchSocketPolicy(serverName, serverPort, 500)) 
			{
				Debug.LogError("Security Exception. Policy file loading failed!");
			}
		}
		

		
		smartFox = new SmartFox(true);
		smartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
		smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
		smartFox.AddEventListener(SFSEvent.LOGIN, OnLogin);
        smartFox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        smartFox.AddEventListener(SFSEvent.LOGOUT, OnLogout);
		
		// Also register for all debug messages from the API at the given log level.
		//smartFox.AddLogListener(logLevel, OnDebugMessage);
		
		statusMessage = "Not connected (client API v" + smartFox.Version + ")";
		logMessages.Add("LOG MESSAGES");
		
		smartFox.Connect(serverName, serverPort);
	}
		
		//So that Unity won't start messing up things; let's queue the event.
		void FixedUpdate() 
		{
			smartFox.ProcessEvents();
		}
	
	
	
	//Here the GUI will be rendered every second.
	void OnGUI() 
{
	if (smartFox == null) return;
		
		// Lets just quickly set up some GUI layout variables.
	float loginboxLeft = Screen.width/4;
	float loginboxTop = Screen.height;
	float loginboxWidth = Screen.width/2;
	float loginboxHeight = 200;
		
		
	if(GUIOn == true)
		{
		//Basic login-screen GUI.
		GUILayout.BeginArea(new Rect (0, 0, Screen.width, Screen.height));
		GUI.Label (new Rect (0, 0, Screen.width, Screen.height), background);
		GUILayout.EndArea();
		
		GUILayout.BeginArea(new Rect (loginboxLeft, loginboxTop - 500, loginboxWidth, loginboxHeight));
		GUILayout.Box ("Green Harvest", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
		GUILayout.BeginVertical();
		GUILayout.BeginArea(new Rect(20, 25, loginboxWidth-40, loginboxHeight-100));

		// Input boxes for username and password.			
		//Only allow them to be accessed -after- a connection has been made with the server!
			
		if (Connected == true)
	{
		GUI.SetNextControlName("username");
		username = GUI.TextField(new Rect(40, 20, 100, 20), username, 30);
		
		GUI.SetNextControlName("password");
		password = GUI.PasswordField(new Rect(40, 50, 100, 20), password, "*"[0], 30);
		GUILayout.Label(errorlog);
		if (GUI.GetNameOfFocusedControl () == "username")
		{
			if (username == "Username") username = "";
		}
		else
		{
			if (username == "") 
			{
				if(GUI.GetNameOfFocusedControl () != "username")
				{
					username = "Username";
				}
			}
		}
		
		if (GUI.GetNameOfFocusedControl () == "password")
		{
			if (password == "Password") password = "";
		}
		else
		{
			if (password == "") 
			{
				if(GUI.GetNameOfFocusedControl () != "password");
				{
					password = "Password";
				}
			}
			
			
		}
			
	}
		else
			{
			GUILayout.Label("You are disconnected from the server! Please try restarting the application and check your internet connection. Our servers may be down!");	
			
			}
		
		
		GUILayout.EndArea ();		
		
		GUILayout.BeginArea(new Rect(20, loginboxHeight-70, loginboxWidth-40, 80));
		
		// Display client status (not needed any more?)
		GUIStyle centeredLabelStyle = new GUIStyle(GUI.skin.label);
		centeredLabelStyle.alignment = TextAnchor.MiddleCenter;
		
		
		// Center button
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();		
			// Connect button
		if (Connected == true)
			{
			if (GUILayout.Button("LOG IN"))  
				{
				smartFox.Send(new LoginRequest(username,password, zone));
				}
				
			else if (Event.current.isKey && Event.current.keyCode == KeyCode.Return)
				{

					if (GUI.GetNameOfFocusedControl() == "password")
						{
						smartFox.Send(new LoginRequest(username, password, zone));
						}
				
					else if (GUI.GetNameOfFocusedControl() == "username")
						{
						smartFox.Send(new LoginRequest(username,password, zone));
				
						}

				
				}
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		GUILayout.EndArea();
	
}


	void OnLogin(BaseEvent evt)
		{
			Console.WriteLine("Hi, I have just logged in as: " + ((User)evt.Params["user"]).Name);
            Application.LoadLevel(1);
		}
		
		void OnLoginError(BaseEvent evt)
		{
			Debug.Log("Failed Login...");
			errorlog = "You have entered a wrong username or password!";
		}
		
		void OnLogout(BaseEvent evt){
			Debug.Log("Logging out.");
		}
	
		public void OnConnection(BaseEvent evt) 
		{
		bool success = (bool)evt.Params["success"];
			if (success) 
			{
				Connected = true;
				statusMessage = "Connection successful!";
			} else 
			{
				statusMessage = "Can't connect to server!";
			}
		}
		
		
		

	public void OnConnectionLost(BaseEvent evt) 
	{
		statusMessage = "Connection lost; reason: " + (string)evt.Params["reason"];
	}
	
	//----------------------------------------------------------
	// Show the debug messages both in window as well as console log
	//----------------------------------------------------------
	public void OnDebugMessage(BaseEvent evt) 
	{
		string message = (string)evt.Params["message"];
		logMessages.Add(message);
		Debug.Log("[SFS DEBUG] " + message);
	}
	
	
	
	
	

	
	//----------------------------------------------------------
	// Disconnect from the socket when shutting down the game
	//----------------------------------------------------------
	public void OnApplicationQuit()
	{
		UnregisterSFSSceneCallbacks();
		smartFox.Disconnect();
	}
	
}