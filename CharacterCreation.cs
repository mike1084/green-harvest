﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Logging;




public class CharacterCreation : MonoBehaviour 
{
	public bool Connected = false;
	public bool GUIOn = true;
	public string userName = "Username";
	public string userPass = "Password";
	public Texture2D dwarfTexture;
	public Texture2D male_icon;
	public Texture2D female_icon;
	public string serverName = "84.84.125.182";		// Use Unity Inspector to change this value.
	public int serverPort = 9933;				// Use Unity Inspector to change this value.
	public LogLevel logLevel = LogLevel.DEBUG;	// Use Unity Inspector to change this value.
	public string zone = "TestWorld";
	
	//The Kingdom (Human) vars
	public bool KingdomA = true;
	public bool KingdomB = false;
	public bool KingdomC = false;
	public bool KingdomD = false;
	public bool KingdomE = false;
	public bool KingdomF = false;
	public bool KingdomG = false;
	public bool KingdomLav = false;
	public bool Outlaw = false;
	
	//The village (Giant) vars
	public bool VillageA = true;
	public bool VillageB = false;
	public bool VillageC = false;
	public bool VillageD = false;
	public bool VillageE = false;
	public bool VillageF = false;
	public bool VillageG = false;
	public bool VillageH = false;
	public bool VillageI = false;
	
	//The Empire (Elf) vars
	public bool EmpireA = true;
	public bool EmpireB = false;
	public bool EmpireC = false;
	public bool EmpireD = false;
	public bool EmpireE = false;
	public bool EmpireF = false;
	public bool EmpireG = false;
	public bool EmpireH = false;
	public bool EmpireI = false;
	
	//The basic variables for character creation
	public int gender = 1;
	public int race = 1;
	//The current screen the creation is on.
	public int GUI_Screen = 0;
	public string[] raceSelection = new string[] {"Man", "Giant", "Elf", "Dwarf", "Orc"};
	public string race_name = "Man";
	public string race_description = "Once nomadic tribesmen, the Men have come to settle across much of the midland plains from the northern tundra to the southern savannah. Their blades sing a bloodied song of war and famine, nd their people know no time before the march against the Salaman Empire, a conquest that has seen generations rise from the earth and fade into the dust. The Men span from reach to reach, with legions that adapt to all fields of battle like no other force alive; their swordsmen make men feared, their leaders make men scorned.";
	public string screen_name = "Race selection";
	public string first_name;
	public string last_name;
	public float age = 10.0f;
	public float age_min;
	public float age_max;
	public int hair_colour;
	public string faction_selected;
	
	
	//public Material CharacterOne = Resources.Load("CharacterOne", typeof(Material)) as material;	
	private string statusMessage = "";
	private List<string> logMessages = new List<string>();
	private Vector2 logScrollPosition;	
	
	protected SmartFox smartFox;
	public bool debug = true;

	
	//Allow the program to remain working even while un-focused.
	protected void Awake(){
		Application.runInBackground = true;
	}
	//Stop any connections / callbacks.
	protected void UnregisterSFSSceneCallbacks(){
		smartFox.RemoveAllEventListeners();
		
	}
	
	// Use this for initialization.
	void Start()
	{
		
		if (Application.isWebPlayer || Application.isEditor) 
		{
			if (!Security.PrefetchSocketPolicy(serverName, serverPort, 500)) 
			{
				Debug.LogError("Security Exception. Policy file loading failed!");
			}
		}

		
		smartFox = new SmartFox(true);
		smartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
		smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
		
		// Also register for all debug messages from the API at the given log level.
		//smartFox.AddLogListener(logLevel, OnDebugMessage);
		
		statusMessage = "Not connected (client API v" + smartFox.Version + ")";
		logMessages.Add("LOG MESSAGES");
		
		smartFox.Connect(serverName, serverPort);
	}
		
		//So that Unity won't start messing up things; let's queue the event.
		void FixedUpdate() 
	{
			smartFox.ProcessEvents();
	}
	
	
	
	//Here the GUI will be rendered every second.
	void OnGUI() 
	{
		if (smartFox == null) return;
		
	// Lets just quickly set up some GUI layout variables.
	float GUIAreaTop = Screen.height * 0.2f;
	float GUIAreaLeft = Screen.width * 0.15f;
	float GUIOverlayWidth = Screen.width * 0.7f;
	float GUIOverlayHeight = Screen.height * 0.6f;
	float GUIInputAreaTop = Screen.height * 0.05f;
	float GUIInputAreaLeft = Screen.width * 0.05f;
	float GUIInputAreaWidth = Screen.width * 0.6f;
	float GUIInputAreaHeight = Screen.height * 0.5f;
	float raceSelectionToolbarLeft = Screen.width * 0.15f;
	float raceSelectionToolbarHeight = Screen.height * 0.05f;
	float raceSelectionToolbarTop = Screen.height * 0.35f;
	float raceSelectionToolbarWidth = Screen.width * 0.45f;
	float raceSelectionTextWidth = Screen.width * 0.4f;
	float raceSelectionTextHeight = Screen.height * 0.25f;
	float ButtonWidth = Screen.width * 0.1f;
	float ButtonHeight = Screen.height * 0.05f;
	float ButtonPrevLeft = Screen.width * 0f;
	float ButtonPrevTop = Screen.height * 0.35f;
	float ButtonNextLeft = Screen.width * 0.5f;
	float ButtonNextTop = Screen.height * 0.35f;
	float ToggleWidth = Screen.width * 0.15f;
	float ToggleHeight = Screen.height * 0.025f;
	float ToggleOneTop = Screen.height * 0.025f;
	float ToggleTwoTop = Screen.height * 0.05f;
	float ToggleThreeTop = Screen.height * 0.075f;
	float ToggleFourTop = Screen.height * 0.1f;
	float ToggleFiveTop = Screen.height * 0.125f;
	float ToggleSixTop = Screen.height * 0.15f;
	float ToggleSevenTop = Screen.height * 0.175f;
	float ToggleEightTop = Screen.height * 0.2f;
		
		
		
		
	if(GUIOn = true)
	{
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), dwarfTexture);
		GUILayout.BeginArea(new Rect(GUIAreaLeft, GUIAreaTop, GUIOverlayWidth, GUIOverlayHeight));
		GUI.Box(new Rect(0,0,GUIOverlayWidth,GUIOverlayHeight), screen_name);
		GUILayout.BeginArea(new Rect(GUIInputAreaLeft, GUIInputAreaTop, GUIInputAreaWidth, GUIInputAreaHeight));
		
			
			//Race select screen	
			if(GUI_Screen == 0)
			{
			GUI.Label(new Rect(GUIInputAreaLeft, GUIInputAreaTop, raceSelectionTextWidth, raceSelectionTextHeight), race_description);
			race = GUI.Toolbar(new Rect(0, raceSelectionToolbarTop, raceSelectionToolbarWidth, raceSelectionToolbarHeight), race, raceSelection);
			if (GUI.Button (new Rect (ButtonNextLeft,ButtonNextTop,ButtonWidth,ButtonHeight), "Next 1"))
			{
				GUI_Screen = 1;
				screen_name = "Faction selection";
			}
			
			}
		//Faction select screen
		else if(GUI_Screen == 1)
		{
			//If humans chosen:
			if(race == 0)
			{
				if(GUI.Toggle(new Rect(0,0,ToggleWidth,ToggleHeight), KingdomA, "Kingdom of A"))
				{
					KingdomA = true;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom A";
				}
				if(GUI.Toggle(new Rect(0,ToggleOneTop,ToggleWidth,ToggleHeight), KingdomB, "Kingdom of B"))
				{
					KingdomA = false;
					KingdomB = true;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom B";
				}
				if(GUI.Toggle(new Rect(0,ToggleTwoTop,ToggleWidth,ToggleHeight), KingdomC, "Kingdom of C"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = true;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom C";
				}
				if(GUI.Toggle(new Rect(0,ToggleThreeTop,ToggleWidth,ToggleHeight), KingdomD, "Kingdom of D"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = true;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom D";
				}
				if(GUI.Toggle(new Rect(0,ToggleFourTop,ToggleWidth,ToggleHeight), KingdomE, "Kingdom of E"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = true;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom E";
				}
				if(GUI.Toggle(new Rect(0,ToggleFiveTop,ToggleWidth,ToggleHeight), KingdomF, "Kingdom of F"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = true;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom F";
				}
				if(GUI.Toggle(new Rect(0,ToggleSixTop,ToggleWidth,ToggleHeight), KingdomG, "Kingdom of G"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = true;
					KingdomLav = false;
					Outlaw = false;
					faction_selected = "Kingdom G";
				}
				if(GUI.Toggle(new Rect(0,ToggleSevenTop,ToggleWidth,ToggleHeight), KingdomLav, "The Lavender Colony"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = true;
					Outlaw = false;
					faction_selected = "The Lavender Colony";
				}
				if(GUI.Toggle(new Rect(0,ToggleEightTop,ToggleWidth,ToggleHeight), Outlaw, "Outlaw"))
				{
					KingdomA = false;
					KingdomB = false;
					KingdomC = false;
					KingdomD = false;
					KingdomE = false;
					KingdomF = false;
					KingdomG = false;
					KingdomLav = false;
					Outlaw = true;
					faction_selected = "Outlaw";
				}
				

				
			}
			//If Giants chosen:
			else if(race == 1)
			{
				if(GUI.Toggle(new Rect(0,0,ToggleWidth,ToggleHeight), VillageA, "Village A"))
				{
					VillageA = true;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village A";
				}
				if(GUI.Toggle(new Rect(0,ToggleOneTop,ToggleWidth,ToggleHeight), VillageB, "Village B"))
				{
					VillageA = false;
					VillageB = true;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village B";
				}
				if(GUI.Toggle(new Rect(0,ToggleTwoTop,ToggleWidth,ToggleHeight), VillageC, "Village C"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = true;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village C";
				}
				if(GUI.Toggle(new Rect(0,ToggleThreeTop,ToggleWidth,ToggleHeight), VillageD, "Village D"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = true;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village D";
				}
				if(GUI.Toggle(new Rect(0,ToggleFourTop,ToggleWidth,ToggleHeight), VillageE, "Village E"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = true;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village E";
				}
				if(GUI.Toggle(new Rect(0,ToggleFiveTop,ToggleWidth,ToggleHeight), VillageF, "Village F"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = true;
					VillageG = false;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village F";
				}
				if(GUI.Toggle(new Rect(0,ToggleSixTop,ToggleWidth,ToggleHeight), VillageG, "Village G"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = true;
					VillageH = false;
					VillageI = false;
					faction_selected = "Village G";
				}
				if(GUI.Toggle(new Rect(0,ToggleSevenTop,ToggleWidth,ToggleHeight), VillageH, "Village H"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = true;
					VillageI = false;
					faction_selected = "Village H";
				}
				if(GUI.Toggle(new Rect(0,ToggleEightTop,ToggleWidth,ToggleHeight), VillageI, "Village I"))
				{
					VillageA = false;
					VillageB = false;
					VillageC = false;
					VillageD = false;
					VillageE = false;
					VillageF = false;
					VillageG = false;
					VillageH = false;
					VillageI = true;
					faction_selected = "Village I";
				}
			}
			
			//if Elves chosen
			else if(race == 2)
			{
				if(GUI.Toggle(new Rect(0,0,ToggleWidth,ToggleHeight), EmpireA, "Empire A"))
				{
					EmpireA = true;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "Empire A";
				}
				if(GUI.Toggle(new Rect(0,ToggleOneTop,ToggleWidth,ToggleHeight), EmpireB, "The Wanderers"))
				{
					EmpireA = false;
					EmpireB = true;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "The Wanderers";
				}
				if(GUI.Toggle(new Rect(0,ToggleTwoTop,ToggleWidth,ToggleHeight), EmpireC, "The Lost Empire"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = true;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "The Lost Empire";
				}
				if(GUI.Toggle(new Rect(0,ToggleThreeTop,ToggleWidth,ToggleHeight), EmpireD, "Empire D"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = true;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "Empire D";
				}
				if(GUI.Toggle(new Rect(0,ToggleFourTop,ToggleWidth,ToggleHeight), EmpireE, "Empire E"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = true;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "Empire E";
				}
				if(GUI.Toggle(new Rect(0,ToggleFiveTop,ToggleWidth,ToggleHeight), EmpireF, "Empire F"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = true;
					EmpireG = false;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "Empire F";
				}
				if(GUI.Toggle(new Rect(0,ToggleSixTop,ToggleWidth,ToggleHeight), EmpireG, "Empire G"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = true;
					EmpireH = false;
					EmpireI = false;
					faction_selected = "Empire G";
				}
				if(GUI.Toggle(new Rect(0,ToggleSevenTop,ToggleWidth,ToggleHeight), EmpireH, "Empire H"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = true;
					EmpireI = false;
					faction_selected = "Empire H";
				}
				if(GUI.Toggle(new Rect(0,ToggleEightTop,ToggleWidth,ToggleHeight), EmpireI, "Empire I"))
				{
					EmpireA = false;
					EmpireB = false;
					EmpireC = false;
					EmpireD = false;
					EmpireE = false;
					EmpireF = false;
					EmpireG = false;
					EmpireH = false;
					EmpireI = true;
					faction_selected = "Empire I";
				}
			}
			
			if (GUI.Button (new Rect(0,ButtonPrevTop,ButtonWidth,ButtonHeight), "Previous 1"))
			{
				GUI_Screen = 0;
				screen_name = "Race selection";
			}
			if (GUI.Button (new Rect (ButtonNextLeft,ButtonNextTop,ButtonWidth,ButtonHeight), "Next 2"))
			{
				GUI_Screen = 2;
				screen_name = "Gender selection";
			}
			
		}
		//Gender select screen
		else if(GUI_Screen == 2)
		{
			if(GUI.Button (new Rect(0,ButtonPrevTop,ButtonWidth,ButtonHeight), "Previous 2"))
			{
				GUI_Screen = 1;
				screen_name = "Faction selection";
			}
			
			if(GUI.Button (new Rect(0,0,100,50), "male_icon"))
			{
				gender = 1;
			}
			if(GUI.Button (new Rect(0,60,100,50), "female_icon"))
			{
				gender = 0;
			}
			
			if (GUI.Button (new Rect (ButtonNextLeft,ButtonNextTop,ButtonWidth,ButtonHeight), "Next 3"))
			{
				GUI_Screen = 3;
				screen_name = "Age selection";
			}
		}
		//Age select screen
		else if(GUI_Screen == 3)
		{
			if(GUI.Button (new Rect(0,ButtonPrevTop,ButtonWidth,ButtonHeight), "Previous 3"))
			{
				GUI_Screen = 2;
				screen_name = "Gender selection";
			}
			
			age = GUI.HorizontalSlider(new Rect(0,0,100,30), age, age_min, age_max);
			age = (int) age;
			if(age == 0)
			{
				age = 10;
			}
			
			GUI.Label(new Rect(0,50,100,30), "Age: " + age.ToString());
			
			if (GUI.Button (new Rect (ButtonNextLeft,ButtonNextTop,ButtonWidth,ButtonHeight), "Next 4"))
			{
				GUI_Screen = 4;
				screen_name = "Name";
			}
		}
		//Name input screen
		else if(GUI_Screen == 4)
		{
			if(GUI.Button (new Rect(0,ButtonPrevTop,ButtonWidth,ButtonHeight), "Previous 4"))
			{
				GUI_Screen = 3;
				screen_name = "Age selection";
			}
		}
		
		GUILayout.EndArea();
		GUILayout.EndArea();
		//If man	
		if(race == 0)
		{
			race_name = "Man";
			race_description = "Once nomadic tribesmen, the Men have come to settle across much of the midland plains from the northern tundra to the southern savannah. Their blades sing a bloodied song of war and famine, nd their people know no time before the march against the Salaman Empire, a conquest that has seen generations rise from the earth and fade into the dust. The Men span from reach to reach, with legions that adapt to all fields of battle like no other force alive; their swordsmen make men feared, their leaders make men scorned.";
			age_min = 10f;
			age_max = 65f;
			if(age > 65)
			{
				age = 10f;
			}
			
		}
		//If Giant
		else if(race == 1)
		{
			race_name = "Giant";
			race_description = "You are a giant!";
			age_min = 10f;
			age_max = 130f;
			if(age > 130)
			{
				age = 10f;
			}
		}
		//If Elf
		else if(race == 2)
		{
			race_name = "Elf";
			race_description = "You are an elf!";
			age_min = 20f;
			age_max = 200f;
			if(age < 20)
			{
				age = 20f;
			}
		}
		//If Dwarf
		else if(race == 3)
		{
			race_name = "Dwarf";
			race_description = "You are a dwarf!";
			age_min = 10;
			age_max = 150;
			if(age > 150)
			{
				age = 10;
			}
		}
		//If Orc
		else if(race == 4)
		{
			race_name = "Orc";
			race_description = "You are an orc!";
			age_min = 10;
			age_max = 130;
			if(age > 130)
			{
				age = 10;
			}
		}
	}
		
}
			
public void OnConnection(BaseEvent evt) 
	{
		bool success = (bool)evt.Params["success"];
		if (success) 
		{
			Connected = true;
			statusMessage = "Connection successful!";
		} else 
		{
			statusMessage = "Can't connect to server!";
		}
	}

	public void OnConnectionLost(BaseEvent evt) 
	{
		statusMessage = "Connection lost; reason: " + (string)evt.Params["reason"];
	}
	

	
	//----------------------------------------------------------
	// Show the debug messages both in window as well as console log
	//----------------------------------------------------------
	public void OnDebugMessage(BaseEvent evt) 
	{
		string message = (string)evt.Params["message"];
		logMessages.Add(message);
		Debug.Log("[SFS DEBUG] " + message);
	}
	
	
	

	
	//----------------------------------------------------------
	// Disconnect from the socket when shutting down the game
	//----------------------------------------------------------
	public void OnApplicationQuit() 
	{
		UnregisterSFSSceneCallbacks();
		smartFox.Disconnect();
	}
	
}